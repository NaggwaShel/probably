-- ProbablyEngine Rotations
-- Released under modified BSD, see attached LICENSE.

local L = ProbablyEngine.locale.get

local DiesalTools = LibStub("DiesalTools-1.0")
local DiesalStyle = LibStub("DiesalStyle-1.0") 
local DiesalGUI = LibStub("DiesalGUI-1.0")
local DiesalMenu = LibStub("DiesalMenu-1.0")
local SharedMedia = LibStub("LibSharedMedia-3.0")

ProbablyEngine.combatTrackerGUI = { }

function ProbablyEngine.combatTrackerGUI.init()

  local dCombatTracker = DiesalGUI:Create('Window')
  ProbablyEngine.combatTrackerGUI.window = dCombatTracker

  ProbablyEngine.interface.makeSavable(dCombatTracker, 'dCombatTracker')

  dCombatTracker:SetWidth(250)
  dCombatTracker:SetHeight(200)
  dCombatTracker:Hide()

  dCombatTracker:SetTitle("|cff"..ProbablyEngine.addonColor..L('combat_tracker').."|r")

  local dCombatTrackerScroll = DiesalGUI:Create('ScrollFrame')
  dCombatTracker:AddChild(dCombatTrackerScroll)
  dCombatTrackerScroll:SetParent(dCombatTracker.content)
  dCombatTrackerScroll:SetPoint("TOPLEFT", dCombatTracker.content, "TOPLEFT", -3, 1)
  dCombatTrackerScroll:SetPoint("BOTTOMRIGHT", dCombatTracker.content, "BOTTOMRIGHT", 2, 0)
  dCombatTrackerScroll.parent = dCombatTracker

  dCombatTracker:ApplySettings()
  ProbablyEngine.interface.makeSavable(dCombatTracker, 'PE_CombatTracker')

  local statusBars = { }
  local statusBarsUsed = { }

  local function getStatusBar()
    local statusBar = tremove(statusBars)
    if not statusBar then
      statusBar = DiesalGUI:Create('StatusBar')
      dCombatTrackerScroll:AddChild(statusBar)
      statusBar:SetParent(dCombatTrackerScroll.content)
      statusBar.frame:SetStatusBarColor(DiesalTools:GetColor(ProbablyEngine.addonColor))
    end
    statusBar:Show()
    table.insert(statusBarsUsed, statusBar)
    return statusBar
  end

  local function recycleStatusBars()
    for i = #statusBarsUsed, 1, -1 do
      statusBarsUsed[i]:Hide()
      tinsert(statusBars, tremove(statusBarsUsed))
    end
  end

  ProbablyEngine.timer.register("updateCTHealthUI", function()

    recycleStatusBars()

    local currentRow = 0

    dCombatTracker:SetTitle("|cff"..ProbablyEngine.addonColor..L('combat_tracker').."|r", ProbablyEngine.module.combatTracker.totalUnits .. " Units")

    if ProbablyEngine.interface.fetchKey('coreConfig', 'ct_total', true) then
      
      local totalHP = 0
      local totalCurrentHP = 0
      local totalDPS = 0
      local totalPercent = 0
      local totalRemaining = 0
      local totalDeathIn = 0

      for object, combatTime in pairs(ProbablyEngine.module.combatTracker.units) do
        totalHP = totalHP + UnitHealthMax(object)
        totalCurrentHP = totalCurrentHP + UnitHealth(object)
        totalDPS = totalDPS + ProbablyEngine.module.combatTracker.dps[object]
        totalDeathIn = totalDeathIn + ProbablyEngine.module.combatTracker.ttd[object]
      end

      totalDeathIn = totalDeathIn / ProbablyEngine.module.combatTracker.totalUnits

      if totalCurrentHP > 0 and totalHP > 0 then

        totalPercent = math.floor(((totalCurrentHP / totalHP) * 100))
        totalRemaining = math.floor(totalCurrentHP / 1000)
        total_deaht_in = string.format("%.2d:%.2d", totalDeathIn/60, totalDeathIn%60)

        local statusBar = getStatusBar()
        statusBar.frame:SetPoint("TOPRIGHT", dCombatTrackerScroll.frame, "TOPRIGHT", -2, -1 + (currentRow * -15) + -currentRow )
        statusBar.frame:SetPoint("TOPLEFT", dCombatTrackerScroll.frame, "TOPLEFT", 2, -1 + (currentRow * -15) + -currentRow )
        statusBar:SetValue(totalPercent)
        statusBar.frame.Left:SetText(L('all_units'))
        statusBar.frame.Right:SetText(totalPercent .. '%' .. ' '  .. math.round(totalDPS/1000, 1) .. 'k DPS ' ..  '( ' .. total_deaht_in .. ' ) ')

        currentRow = currentRow + 1

      end

    end

    for object, combatTime in pairs(ProbablyEngine.module.combatTracker.units) do

      local statusBar = getStatusBar()

      local name = UnitName(object)
      local health = UnitHealth(object)
      local maxHealth = UnitHealthMax(object)
      local dps = ProbablyEngine.module.combatTracker.dps[object]

      statusBar.frame:SetPoint("TOPRIGHT", dCombatTrackerScroll.frame, "TOPRIGHT", -1, -1 + (currentRow * -15) + -currentRow )
      statusBar.frame:SetPoint("TOPLEFT", dCombatTrackerScroll.frame, "TOPLEFT", 2, -1 + (currentRow * -15) + -currentRow )

      statusBar.frame.Left:SetText(name)

      if health and maxHealth then
        local percent = math.floor(((health / maxHealth) * 100))
        local remaining = math.floor(health / 1000)
        -- We have the unitHP and max HP
        -- show a nice % bar
        statusBar:SetValue(percent)
        if ProbablyEngine.module.combatTracker.ttd[object] then
          seconds = ProbablyEngine.module.combatTracker.ttd[object]
          deaht_in = string.format("%.2d:%.2d", seconds/60, seconds%60)
        else
          deaht_in = L('est')
        end

        local text = ''

        if ProbablyEngine.interface.fetchKey('coreConfig', 'ct_prct', true) then
          text = text .. '(' .. percent .. '%) '
        end

        if ProbablyEngine.interface.fetchKey('coreConfig', 'ct_hp', false) then
          if health < 1000 then
            text = text .. health .. ' '
          else
            text = text .. math.round(health / 1000, 1) .. 'k '
          end
        end

        if ProbablyEngine.interface.fetchKey('coreConfig', 'ct_dps', true) then
          if dps < 1000 then
            text = text .. math.round(dps, 1) .. ' DPS '
          else
            text = text .. math.round(dps/1000, 1) .. 'k DPS '
          end
        end

        if ProbablyEngine.interface.fetchKey('coreConfig', 'ct_ttd', true) then
          text = text .. '( ' .. deaht_in .. ' ) '
        end

        statusBar.frame.Right:SetText(text)
      end

      currentRow = currentRow + 1
    end


    --for i = currentRow, max_tracking do
    --  CombatTableUnit[i]:SetValue(0)
    --  CombatTableUnit[i].unitName:SetText('')
    --  CombatTableUnit[i].unitHealth:SetText('')
    --  CombatTableUnit[i].unit = false
    --end

  end, 100)

end
