-- ProbablyEngine Rotations
-- Released under modified BSD, see attached LICENSE.

-- im 100% nigga


ProbablyEngine.module.register("combatTracker", {
  units = { },
  ttd = { },
  dps = { },
  health = { },
  totalUnits = 0,
})

local totalUnits = ProbablyEngine.module.combatTracker.totalUnits
local units = ProbablyEngine.module.combatTracker.units
local ttd = ProbablyEngine.module.combatTracker.ttd
local dps = ProbablyEngine.module.combatTracker.dps
local health = ProbablyEngine.module.combatTracker.health



if FireHack then
    ProbablyEngine.timer.register("updateCTData", function()
        local totalObjects = ObjectCount()
        for i = 1, totalObjects do
            local object = ObjectWithIndex(i)
            if UnitAffectingCombat(object) and UnitIsTappedByPlayer(object) and not units[object] then
                units[object] = GetTime()
                health[object] = UnitHealth(object)
                dps[object] = 0
                if ttd[object] == nil then
                    ttd[object] = -1 
                end
                ProbablyEngine.module.combatTracker.totalUnits = ProbablyEngine.module.combatTracker.totalUnits + 1
            elseif not UnitAffectingCombat(object) and UnitIsTappedByPlayer(object) and units[object] then
                units[object] = nil
                ttd[object] = nil
                health[object] = nil
                dps[object] = nil
                ProbablyEngine.module.combatTracker.totalUnits = ProbablyEngine.module.combatTracker.totalUnits - 1
            else
                if units[object] and ttd[object] then
                    local currentHP = UnitHealth(object)
                    local maxHP = health[object]
                    local diff = maxHP - currentHP
                    local dura = GetTime() - units[object]
                    local _dps = diff / dura
                    local death = currentHP / _dps
                    dps[object] = math.floor(_dps)
                    if death == math.huge then
                        ttd[object] = -1
                    elseif death < 0 then
                        ttd[object] = 0
                    else
                        ttd[object] = death
                    end
                end
            end
        end
        for object, _ in pairs(units) do
            if not UnitExists(object) then
                units[object] = nil
                ttd[object] = nil
                health[object] = nil
                dps[object] = nil
                ProbablyEngine.module.combatTracker.totalUnits = ProbablyEngine.module.combatTracker.totalUnits - 1
            end
        end
    end, 100)
end